import static org.junit.Assert.*;

import org.junit.Test;



import PluginParticipaBR.Reivindicacao;
import PluginParticipaBR.Model.PessoaBr;
public class PessoaBrTest {

	@Test
	public void test() {
		fail("Not yet implemented");
	}

}
package test;

import static org.junit.Assert.*;

import model.Aluno;
import model.Disciplina;

import org.junit.Before;
import org.junit.Test;

public class TestModel {
	
	private PessoaBr umaPessoaBr;
	private Reivindicacao umaReivindicacao;
	
	@Before
	public void setUp()throws Exception{
		umaPessoaBr = new PessoaBr();
		umaReivindicacao = new Reivindicacao();
	}

	@Test
	public void testNome() {
		umaPessoaBr.setNome("xxxxxxx");
		assertEquals("xxxxxxx", umaPessoaBr.getNome());
		
	}
	
	@Test
	public void testEmail() {
		umaPessoaBr.setEmail("xxx@xxx.com");
		assertEquals("xxx@xxx.com", umaPessoaBr.getEmail());
		
	}
	
	@Test
	public void testEstado() {
		umaPessoaBr.setEstado("xx");
		assertEquals("xx", umaPessoaBr.getEstado());
		
	}
	
	@Test
	public void testProfissao() {
		umaPessoaBr.setProfissao("xxxxx");
		assertEquals("xxxxx", umaPessoaBr.getProfissao());
		
	}
	
	@Test
	public void testIdade() {
		umaPessoaBr.setIdade("xx");
		assertEquals("xx", umaPessoaBr.getIdade());
		
	}
	
}